<?php 
require_once('session.php');
require_once('db.php'); 
$_SESSION['school'] = 'columbia';
$_SESSION['LASTURL'] = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$meta_description = "metaCollege is launching at Columbia University. Sell your textbooks to your peers for more money at the end of this semester!";
$meta_title = "metaCollege - Textbook Marketplace at Columbia University";
$meta_siteName = "metaCollege.com";
$meta_image = "https://www.metacollege.com/img/white_bg.jpg"
?>

<!DOCTYPE html>

<html lang="en">

  <head>

    <!-- SITE TITTLE -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1"> 

    <?php include_once("include/metatags.php"); ?>

    
    <title>metaCollege - Textbook Marketplace at Columbia University</title>



    <!-- PLUGINS CSS STYLE -->

    <link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">

    <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="plugins/selectbox/select_option1.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="plugins/rs-plugin/css/settings.css" media="screen">

    <link rel="stylesheet" type="text/css" href="plugins/owl-carousel/owl.carousel.css" media="screen">



    <!-- GOOGLE FONT -->

    <link href='https://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>



    <!-- linearicons -->

    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">



    <!-- CUSTOM CSS -->

    <link href="css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="css/default.css" id="option_color">



    <!-- Icons -->

    <link rel="shortcut icon" href="img/favicon.png"> 



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <?php include_once("googleanalytics.php"); ?>


  </head>



  <body class="body-wrapper">

    <?php include_once('signin_modals.php'); ?>


    <div class="main-wrapper">



      <?php include_once('nav.php'); ?>



      <!-- main content -->

      
      <!-- BANNER -->

      <div class="bannercontainer bannerV1">

        <div class="fullscreenbanner-container">

          <div class="fullscreenbanner">

            <ul>

              <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700"  data-title="Slide 1">

                <img src="img/home/banner-slider/cover1.jpg" alt="slidebg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                <div class="slider-caption slider-captionV1 container">


                  <div class="tp-caption rs-caption-2 sft start"

                    data-hoffset="0"

                    data-y="119"

                    data-x="800"

                    data-speed="800"

                    data-start="1000"

                    data-easing="Back.easeInOut"

                    data-endspeed="300" style="color: #ededed; text-transform: none;">

                    Buy more for less<br> Sell less for more  

                  </div>



                  <div class="tp-caption rs-caption-3 sft"

                    data-hoffset="0"

                    data-y="185"

                    data-x="800"

                    data-speed="1000"

                    data-start="2000"

                    data-easing="Power4.easeOut"

                    data-endspeed="300"

                    data-endeasing="Power1.easeIn"

                    data-captionhidden="off"

                    style="text-transform: none;">

                    <br>

                    Only on metaCollege

                  </div>



                  <div class="tp-caption rs-caption-4 sft"

                    data-hoffset="0"

                    data-y="300"

                    data-x="800"

                    data-speed="800"

                    data-start="2500"

                    data-easing="Power4.easeOut"

                    data-endspeed="300"

                    data-endeasing="Power1.easeIn"

                    data-captionhidden="off">

                    <?php if (!$logged_in) {?><span class="page-scroll"><a data-toggle="modal" href='#signup' class="btn btn-primary-filled">Sign Up<i class="glyphicon glyphicon-chevron-right" style="color: white;"></i></a></span><?php } else {?><span class="page-scroll"><a href='https://www.metacollege.com/account/sell' class="btn btn-primary-filled">Sell Now<i class="glyphicon glyphicon-chevron-right" style="color: white;"></i></a></span><?php } ?>

                  </div>

                </div>

              </li>

              <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="1000" data-title="Slide 2">

                <img src="img/almamater.jpg" alt="slidebg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                <div class="slider-caption slider-captionV1 container captionCenter">

                  <div class="tp-caption rs-caption-1 sft start text-center"

                    data-x="center"

                    data-y="228"

                    data-speed="800"

                    data-start="1500"

                    data-easing="Back.easeInOut"

                    data-endspeed="300">

                    <!-- <img src="img/home/banner-slider/shoe2.png" alt="slider-image"> -->

                  </div>



                  <!-- <div class="tp-caption rs-caption-2 sft text-center"

                    data-x="center"

                    data-y="100"

                    data-speed="800"

                    data-start="2000"

                    data-easing="Back.easeInOut"

                    data-endspeed="300">

                    Welcome to metaCollege

                  </div> -->



                  <div class="tp-caption rs-caption-3 sft text-center" 

                    data-x="center"

                    data-y="150"

                    data-speed="800"

                    data-start="1500"

                    data-easing="Power4.easeOut"

                    data-endspeed="300"

                    data-endeasing="Power1.easeIn"

                    data-captionhidden="off">

                    Hello Columbia

                  </div>

                  <!-- <div class="tp-caption rs-caption-3 sft text-center"

                    data-x="center"

                    data-y="180"

                    data-speed="800"

                    data-start="3000"

                    data-easing="Power4.easeOut"

                    data-endspeed="300"

                    data-endeasing="Power1.easeIn"

                    data-captionhidden="off" style="font-size: 15px;">

                    Welcome to metaCollege

                  </div> -->



                  <div class="tp-caption rs-caption-4 sft text-center"

                    data-x="center"

                    data-y="300"

                    data-speed="800"

                    data-start="2700"

                    data-easing="Power4.easeOut"

                    data-endspeed="300"

                    data-endeasing="Power1.easeIn"

                    data-captionhidden="off">

                    <?php if (!$logged_in) {?><span class="page-scroll"><a data-toggle="modal" href='#signup' class="btn btn-info-filled" style="border-color: #4da5e2;">Sign Up<i class="glyphicon glyphicon-chevron-right" style="color: white;"></i></a></span><?php } else {?><span class="page-scroll"><a href='https://www.metacollege.com/account/sell' class="btn btn-primary-filled">Sell Now<i class="glyphicon glyphicon-chevron-right" style="color: white;"></i></a></span><?php } ?>  

                  </div>

                </div>

              </li>

             

            </ul>

          </div>

        </div>

      </div>


      <!-- container below banner -->
      <section class="mainContent clearfix">

        <div class="container">

          <!-- Feature product row -->
          <div class="row featuredProducts">
            <div class="col-xs-12">
              <div class="page-header">
                <h4>Featured Products</h4>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="owl-carousel featuredProductsSlider">

                <?php  
                $insSql = "SELECT * FROM items WHERE `status` = 'submitted' ORDER BY `itemID` DESC LIMIT 15";
                $insResult = mysqli_query($conn, $insSql);
                while ($insRow = mysqli_fetch_array($insResult)) {
                  $itemID = $insRow['itemID'];
                  $img = $insRow['img1'];
                  $itemTitle = $insRow['item_name'];
                  $price = $insRow['price'];
                  $username = $insRow['username'];
                  $view_count = $insRow['view_count'];
                  $sellerID = $insRow['userID'];
                  $itemType = $insRow['itemType'];
                
                ?>


                <div class="slide">
                  <div class="productImage clearfix" onclick="location.href='item?i=<?=base64_encode($itemID)?>';">

                    <img src="image?f=ii_<?=$img?>" alt="featured-product-img">

                    <div class="productMasking">

                      <ul class="list-inline btn-group" role="group">

                        <li><a class="btn viewBtn" style="width: 150px;"> <span><i class="fa fa-heart"></i> View Product </span></a></li>

                      </ul>

                    </div>

                  </div>

                  <div class="productCaption clearfix">

                    <h5><a href="item?i=<?=base64_encode($itemID)?>"><?=$itemTitle?></a></h5>

                    <h3>$<?=$price?></h3>

                    <h6>From <?=$username?> | <?=$view_count?> view<?php if ($view_count != 1) echo 's'; ?> </h6>

                  </div>
                </div>

                <?php } ?>

                
              </div>
            </div>
          </div>

          <!-- CATEGORIES SECTION -->

          <?php include_once('featuredCollections.php'); ?>
          


          <!-- FEATURE PRODUCT SECTION -->

          <div class="row featuredProducts version3">

            <div class="col-xs-12">

              <div class="tabCommon">

                <ul class="nav nav-tabs">

                  <li class="active"><a data-toggle="tab" href="#menu1">New Arrivals</a></li>

                  <li><a data-toggle="tab" href="#menu2">Popular</a></li>

                  <li><a data-toggle="tab" href="#menu3">On Sale</a></li>

                </ul>

                <div class="tab-bottom">

                </div>

                <div class="tab-content">

                  <div id="menu1" class="tab-pane fade in active">

                    <?php  
                    $newSql = "SELECT * FROM items WHERE `status` = 'submitted' ORDER BY `itemID` DESC LIMIT 8";
                    $newResult = mysqli_query($conn, $newSql);
                    $count = 0;
                    while ($newRow = mysqli_fetch_array($newResult)) {
                      
                      $itemID = $newRow['itemID'];
                      $img = $newRow['img1'];
                      $itemTitle = $newRow['item_name'];
                      $price = $newRow['price'];
                      $username = $newRow['username'];
                      $view_count = $newRow['view_count'];
                      $sellerID = $newRow['userID'];
                      $itemType = $newRow['itemType'];
                    
                    ?>
                    <?php if ($count == 0 || $count == 4) { ?>
                    <div class="row">
                    <?php } ?>
                      

                      <div class="col-sm-3 col-xs-12">

                        <div class="imageBox">

                          <div class="productImage clearfix" onclick="location.href='item?i=<?=base64_encode($itemID)?>';">

                            <img src="image?f=ii_<?=$img?>" alt="featured-product-img">

                            <div class="productMasking">

                              <ul class="list-inline btn-group" role="group">

                                <li><a class="btn viewBtn" style="width: 150px;"> <span><i class="fa fa-heart"></i> View Product </span></a></li>

                              </ul>

                            </div>

                          </div>

                          <div class="productCaption clearfix">

                            <h5><a href="item?i=<?=base64_encode($itemID)?>"><?=$itemTitle?></a></h5>

                            <h3>$<?=$price?></h3>

                            <h6>From <?=$username?> | <?=$view_count?> view<?php if ($view_count != 1) echo 's'; ?> </h6>

                          </div>

                        </div>

                      </div>

                      

                      

                    <?php if ($count == 3 || $count == 7) { ?>
                    </div>
                    <?php } ?>

                    <?php $count++;} ?>

                  </div>

                  <div id="menu2" class="tab-pane fade">

                  <?php  
                  $newSql = "SELECT * FROM items WHERE `status` = 'submitted' ORDER BY `view_count` DESC LIMIT 8";
                  $newResult = mysqli_query($conn, $newSql);
                  $count = 0;
                  while ($newRow = mysqli_fetch_array($newResult)) {
                    
                    $itemID = $newRow['itemID'];
                    $img = $newRow['img1'];
                    $itemTitle = $newRow['item_name'];
                    $price = $newRow['price'];
                    $username = $newRow['username'];
                    $view_count = $newRow['view_count'];
                    $sellerID = $newRow['userID'];
                    $itemType = $newRow['itemType'];
                  
                  ?>
                    <?php if ($count == 0 || $count == 4) { ?>
                    <div class="row">
                    <?php } ?>
                      

                      <div class="col-sm-3 col-xs-12">

                        <div class="imageBox">

                          <div class="productImage clearfix" onclick="location.href='item?i=<?=base64_encode($itemID)?>';">

                            <img src="image?f=ii_<?=$img?>" alt="featured-product-img">

                            <div class="productMasking">

                              <ul class="list-inline btn-group" role="group">

                                <li><a class="btn viewBtn" style="width: 150px;"> <span><i class="fa fa-heart"></i> View Product </span></a></li>

                              </ul>

                            </div>

                          </div>

                          <div class="productCaption clearfix">

                            <h5><a href="item?i=<?=base64_encode($itemID)?>"><?=$itemTitle?></a></h5>

                            <h3>$<?=$price?></h3>

                            <h6>From <?=$username?> | <?=$view_count?> view<?php if ($view_count != 1) echo 's'; ?> </h6>

                          </div>

                        </div>

                      </div>

                      

                      

                    <?php if ($count == 3 || $count == 7) { ?>
                    </div>
                    <?php } ?>

                    <?php $count++;} ?>

                  </div>

                  <div id="menu3" class="tab-pane fade">

                    <?php  
                    $newSql = "SELECT * FROM items WHERE `status` = 'submitted' ORDER BY `price` ASC LIMIT 8";
                    $newResult = mysqli_query($conn, $newSql);
                    $count = 0;
                    while ($newRow = mysqli_fetch_array($newResult)) {
                      $itemID = $newRow['itemID'];
                      $img = $newRow['img1'];
                      $itemTitle = $newRow['item_name'];
                      $price = $newRow['price'];
                      $username = $newRow['username'];
                      $view_count = $newRow['view_count'];
                      $sellerID = $newRow['userID'];
                      $itemType = $newRow['itemType'];
                    
                    ?>
                    <?php if ($count == 0 || $count == 4) { ?>
                    <div class="row">
                    <?php } ?>
                      

                      <div class="col-sm-3 col-xs-12">

                        <div class="imageBox">

                          <div class="productImage clearfix" onclick="location.href='item?i=<?=base64_encode($itemID)?>';">

                            <img src="image?f=ii_<?=$img?>" alt="featured-product-img">

                            <div class="productMasking">

                              <ul class="list-inline btn-group" role="group">

                                <li><a class="btn viewBtn" style="width: 150px;"> <span><i class="fa fa-heart"></i> View Product </span></a></li>

                              </ul>

                            </div>

                          </div>

                          <div class="productCaption clearfix">

                            <h5><a href="item?i=<?=base64_encode($itemID)?>"><?=$itemTitle?></a></h5>

                            <h3>$<?=$price?></h3>

                            <h6>From <?=$username?> | <?=$view_count?> view<?php if ($view_count != 1) echo 's'; ?> </h6>

                          </div>

                        </div>

                      </div>

                      

                      

                    <?php if ($count == 3 || $count == 7) { ?>
                    </div>
                    <?php } ?>

                    <?php $count++;} ?>

                  </div>

                </div>

              </div>

            </div>

          </div>

          <!-- FEATURE -->

          <div class="row features version2">

            <div class="col-sm-4 col-xs-12">

              <div class="box text-center">

                <i class="fa fa-check-circle" aria-hidden="true"></i>

                <h4>Verified</h4>

                <span>Get exactly what you need, directly from student sellers.</span>

              </div>

            </div>

            <div class="col-sm-4 col-xs-12">

              <div class="box text-center">

                <i class="fa fa-lock" aria-hidden="true"></i>

                <h4>Security</h4>

                <span>We protect your money, and your shopping experience.</span>

              </div>

            </div>

            <div class="col-sm-4 col-xs-12">

              <div class="box text-center">
      
                <i class="fa fa-dropbox"></i>

                <h4>Forget About Shipping</h4>

                <span>Meet up and exchange. It's really that easy.</span>

              </div>

            </div>

          </div>


        </div>
      </section>
      <!-- /main content -->

      <!-- FOOTER -->

      <?php include_once('footer.php'); ?>

    </div>


    
    


    <script src="js/jquery.min.js"></script>

    <script src="plugins/jquery-ui/jquery-ui.js"></script>

    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>

    <script src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    <script src="plugins/owl-carousel/owl.carousel.js"></script>

    <script src="plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>

    <script src="plugins/countdown/jquery.syotimer.js"></script>

    <script src="js/custom.js"></script>



  </body>

</html>
