<?php 
require_once('session.php');
require_once('db.php'); 

$itemID = $_GET['i'];
$itemID = base64_decode($itemID);

$_SESSION['LASTURL'] = 'metacollege.com/item?i='.base64_encode($itemID);

$checkSql = "SELECT * FROM items WHERE `itemID` = $itemID";
$result = mysqli_query($conn, $checkSql) or header("location: 404.html");
if (mysqli_num_rows($result) == 0) header("location: 404.html");
else $row = mysqli_fetch_assoc($result);
$course = $row['course'];
$img[1] = $row['img1'];
$img[2] = $row['img2'];
$img[3] = $row['img3'];
$img[4]= $row['img4'];
$img_min[1] = $row['img1_min'];
$img_min[2] = $row['img2_min'];
$img_min[3] = $row['img3_min'];
$img_min[4] = $row['img4_min'];
$itemTitle = $row['item_name'];
$description = $row['description'];
$price = $row['price'];
$view_count = $row['view_count'];
$sellerID = $row['userID'];
$itemType = $row['itemType'];
$status = $row['status'];
$isbn = $row['isbn'];
$isbn10 = $row['isbn10'];
$isbn13 = $row['isbn13'];
$condition = $row['item_condition'];

$getSeller = "SELECT * FROM users WHERE `userID` = $sellerID LIMIT 1";
$result2 = mysqli_query($conn, $getSeller) or header("location: 404.html");
if (mysqli_num_rows($result2) == 0) header("location: 404.html");
else $row2 = mysqli_fetch_assoc($result2);
$sellername = $row2['username'];
// $sellerRating = number_format(round($row2['rating'],1),1);
$availability = $row2['availability'];
$self_description = $row2['self_description'];
$email_verified = $row2['verified'];
$phone_verified = $row2['phone_verified'];
// $transaction_count = $row2['transaction_count'];
$profilepic = $row2['profilepic'];

//calculate seller rating
$getRating = "SELECT * FROM ratings WHERE `sellerID` = $sellerID";
$ratingResult = mysqli_query($conn, $getRating) or die("Something went wrong");
$ratingCount = mysqli_num_rows($ratingResult);
if ($ratingCount == 0) $sellerRating = number_format(0,1);
else {
  $ratingTotal = 0;
  while($ratingRow = mysqli_fetch_array($ratingResult)) {
    $ratingTotal = $ratingTotal + $ratingRow['rating'];
  }
  $sellerRating = number_format(round(($ratingTotal / $ratingCount),1),1);;
}

//calculate transaction count
$getTransaction = "SELECT * FROM orders WHERE `sellerID` = $sellerID AND `status` = 'completed'";
$transResult = mysqli_query($conn, $getTransaction) or die("Something went wrong");
$transaction_count = mysqli_num_rows($transResult);


if ($_SESSION['logged_in']) {
  $getView = "SELECT * FROM views WHERE `itemID` = $itemID AND `userID` = $userID";
  $resultView = mysqli_query($conn, $getView) or die("Something went wrong 1");
  if (mysqli_num_rows($resultView) == 0) {
    $vcPlus = "UPDATE items SET `view_count` = ($view_count+1) WHERE `itemID` = $itemID";
    mysqli_query($conn, $vcPlus) or die("Something went wrong 2");
  }
  $insertView = "INSERT INTO views (`itemID`, `userID`, `username`) VALUES ($itemID, $userID, '$sellername')";
  mysqli_query($conn, $insertView) or die("Something went wrong 3");
}
else {
  // sleep(10);

  $vcPlus = "UPDATE items SET `view_count` = ($view_count+1) WHERE `itemID` = $itemID";
  mysqli_query($conn, $vcPlus) or die("Something went wrong 4");
  
  $addr = $_SERVER['HTTP_CLIENT_IP']?$_SERVER['HTTP_CLIENT_IP']:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']);

  $insertView = "INSERT INTO views (`itemID`, `userID`, `username`) VALUES ($itemID, '', '$addr')"; 
  mysqli_query($conn, $insertView) or die("Something went wrong 5");
}

$meta_description = "metaCollege is launching at Columbia University. Sell your textbooks to your peers for more money at the end of this semester!";
$meta_title = $itemTitle." - metaCollege";
$meta_siteName = "metaCollege.com";
$meta_image = "https://www.metacollege.com/image?f=ii_".$img[1]

?>

<!DOCTYPE html>

<html lang="en">

  <head>

    <?php include_once("googleanalytics.php"); ?>

    <!-- SITE TITTLE -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php include_once("include/metatags.php"); ?>

    <title><?=$itemTitle?> - metaCollege</title>



    <!-- PLUGINS CSS STYLE -->

    <link href="plugins/jquery-ui/jquery-ui.css" rel="stylesheet">

    <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="plugins/selectbox/select_option1.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="plugins/rs-plugin/css/settings.css" media="screen">

    <link rel="stylesheet" type="text/css" href="plugins/owl-carousel/owl.carousel.css" media="screen">



    <!-- GOOGLE FONT -->

    <link href='https://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>



    <!-- CUSTOM CSS -->

    <link href="css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="css/default.css" id="option_color">



    <!-- Icons -->

    <link rel="shortcut icon" href="img/favicon.png">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <script type="text/javascript"> //<![CDATA[ 
    var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
    document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
    //]]>
    </script>

	<style>a[href^=tel] {
    color: inherit;
    text-decoration: none;
}</style>

  </head>



  <body class="body-wrapper">



    <div class="main-wrapper">



      <?php include_once('nav.php'); ?>



      <!-- LIGHT SECTION -->

      <section class="lightSection clearfix pageHeader">

        <div class="container">

          <div class="row">

            <div class="col-xs-6">

              <div class="page-title">

                <h2><?=$itemType?></h2>

              </div>

            </div>

            <div class="col-xs-6">

              <ol class="breadcrumb pull-right">

                <li>

                  <a href="https://www.metacollege.com">Home</a>

                </li>

                <li class="active"><?=$itemType?></li>

              </ol>

            </div>

          </div>

        </div>

      </section>



      <!-- MAIN CONTENT SECTION -->

      <section class="mainContent clearfix">

        <div class="container">

          <?php if (isset($_SESSION['message'])) { ?>
          <div class="alert alert-success fade in alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Message:</strong> <?=$_SESSION['message']?>
          </div>
          <?php unset($_SESSION['message']);} ?>

          <div class="row singleProduct">

            <div class="col-xs-12">

              <div class="media">

                <div class="media-left productSlider">

                  <div id="carousel" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner">
                      <?php
                      $i = 1;
                      while (!empty($img_min[$i]) && $i <= 4) {
                      ?>

                      <div class="item <?php if ($i == 1) echo 'active'; ?> " data-thumb="<?=($i-1)?>">

                        <img src="image?f=ii_<?=$img[$i]?>">

                      </div>

                      <?php $i++; } ?>

                    </div>

                  </div>

                  <div class="clearfix">

                    <div id="thumbcarousel" class="carousel slide" data-interval="false">

                      <div class="carousel-inner">

                          <?php
                          $i = 1;
                          while (!empty($img_min[$i]) && $i <= 4) {
                          ?>
                          <div data-target="#carousel" data-slide-to="<?=($i-1)?>" class="thumb"><img src="image?f=m_<?=$img_min[$i]?>"></div>

                          <?php $i++; } ?>

                      </div>

                      <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">

                        <span class="glyphicon glyphicon-chevron-left"></span>

                      </a>

                      <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">

                        <span class="glyphicon glyphicon-chevron-right"></span>

                      </a>

                    </div>

                  </div>

                </div>

                <div class="media-body">
                
                  <h2><?php if ($status !== 'submitted') echo '[sold] '; ?><?=$itemTitle?></h2>

                  <h3>$<?=$price?>   <a href="https://www.metacollege.com/messenger/?chatwith=<?=$sellerID?>" target="_blank" class="btn btn-xs btn-default" onclick="">Contact Seller</a></h3>
                    
                  <h5 style="margin-bottom: 10px; text-transform: capitalize;">Course: <?=$course?></h5>
                  <?php if (empty($isbn10) || empty($isbn13)) {?>
                  <h5 style="margin-bottom: 10px;">ISBN: <?=$isbn?></h5>
                  <?php } else { ?>
                  <h5 style="margin-bottom: 10px;">ISBN-10: <?=$isbn10?></h5>
                  <h5 style="margin-bottom: 10px;">ISBN-13: <?=$isbn13?></h5>
                  <?php } ?>
                  <h5 style="text-transform: capitalize;">Condition: <?=$condition?></h5>

                  <p><?=$description?></p>

                  <div class="btn-area">

                    <a <?php if ($status !== 'submitted') echo 'disabled'; else {?> href="action?act=wishlist&item=<?=$itemID?>"<?php } ?> class="btn btn-primary" >Wishlist<i class="fa fa-angle-right" aria-hidden="true"></i></a>

                    <a <?php if ($status !== 'submitted') echo 'disabled'; else {?> href="checkout/step-1?in=<?=$itemID?>" <?php } ?> class="btn btn-primary" style="background-color: #437b7d;" <?php if ($status !== 'submitted') echo 'disabled'; ?>>Buy Now<i class="fa fa-angle-right" aria-hidden="true"></i></a>

                    <?php  //echo '<a href="cart-page.html" class="btn btn-primary" style="background-color: #8f908f;">Make Offer<i class="fa fa-angle-right" aria-hidden="true"></i></a>'; ?>

                  </div>

                  <div class="tabArea">

                    <img src="image?f=pp_<?=$profilepic ?>" alt="profile-image" width="120">

                    <ul class="nav nav-tabs">

                      <li class="active"><a data-toggle="tab" href="#details">About seller</a></li>

                    </ul>

                    <div class="tab-content">

                      <div id="details" class="tab-pane fade in active">

                        <p style="margin-bottom: 15px;"><?php if (!empty($self_description)) echo "$self_description"; else echo "I am a very cool person."?></p>

                        <ul class="list-unstyled" style="font-size: 110%;"> 

                          <li>username: <?=$sellername ?></li>

                          <li>ratings:<span style="color: #4dc8cf;">&#9733;<u><?=$sellerRating?></u></span></li>

                          <li>items sold: <?=$transaction_count?></li>

                          <li>meetup preferences: <?php if (empty($availability)) echo "none"; else echo $availability;?></li>

                          <?php if ($phone_verified) {?><li>Phone number verified.</li> <?php } ?>

                          <?php if ($email_verified) {?><li>LionMail verified.</li><?php } ?>

                        </ul>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

          <div class="row productsContent">

            <div class="col-xs-12">

              <div class="page-header">

                <h4>Suggested Items</h4>

              </div>

            </div>

            <?php  
            $newSql = "SELECT * FROM items WHERE `status` = 'submitted' ORDER BY `view_count` DESC LIMIT 4";
            $newResult = mysqli_query($conn, $newSql);
            while ($newRow = mysqli_fetch_array($newResult)) {
              
              $itemID = $newRow['itemID'];
              $img = $newRow['img1'];
              $itemTitle = $newRow['item_name'];
              $price = $newRow['price'];
              $username = $newRow['username'];
              $view_count = $newRow['view_count'];
              $sellerID = $newRow['userID'];
              $itemType = $newRow['itemType'];
            
            ?>
              

            <div class="col-sm-3 col-xs-12">

              <div class="imageBox">

                <div class="productImage clearfix" onclick="location.href='item?i=<?=base64_encode($itemID)?>';">

                  <img src="image?f=ii_<?=$img?>" alt="featured-product-img">

                  <div class="productMasking">

                    <ul class="list-inline btn-group" role="group">

                      <li><a class="btn viewBtn" style="width: 150px;"> <span><i class="fa fa-heart"></i> View Product </span></a></li>

                    </ul>

                  </div>

                </div>

                <div class="productCaption clearfix">

                  <h5><a href="item?s=<?=$sellerID?>&i=<?=$itemID?>&t=<?=$itemType?>"><?=$itemTitle?></a></h5>

                  <h3>$<?=$price?></h3>

                  <h6>From <?=$username?> | <?=$view_count?> view<?php if ($view_count != 1) echo 's'; ?> </h6>

                </div>

              </div>

            </div>

              

              

            <?php } ?>

          </div>

        </div>

      </section>



      <?php include_once('footer.php'); ?>

    </div>



    <?php include_once('signin_modals.php'); ?>




    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script src="plugins/jquery-ui/jquery-ui.js"></script>

    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>

    <script src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    <script src="plugins/owl-carousel/owl.carousel.js"></script>

    <script src="plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>

    <script src="plugins/countdown/jquery.syotimer.js"></script>

    <script src="js/custom.js"></script>

  </body>

</html>

